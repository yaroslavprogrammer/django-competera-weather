from fabric.api import local, task, lcd


@task
def manage(command):
    """Run ./manage.py command"""

    local('./manage.py {}'.format(command))


@task
def run(host='127.0.0.1', port=8000):
    """Start uwsgi development server"""

    manage('runserver {}:{}'.format(host, port))


@task
def clear():
    """Clear temporary files"""

    local(
        "find . -name '~*' -or -name '*.pyo' -or -name '*.pyc' "
        "-or -name '__pycache__' -or -name 'Thubms.db' "
        "| xargs -I {} rm -vrf '{}'")


@task
def edito():
    """Start editor"""

    local('atom .')


@task
def install():
    """Install pip requirements"""

    with lcd('requirements'):
        local('pip install -r development.txt')


@task
def bootstrap():
    """ Bootstrap initial database with project user and permissions """
    local('sh scripts/bootstrap_db.sh')


@task
def init():
    """ Create postgres database, migrate and create user """

    bootstrap()

    manage('migrate')
    manage('createsuperuser')
