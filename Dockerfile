FROM python:3.5

ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

ADD . /app

RUN pip install -r ./requirements/development.txt

ENV PROJECT_NAME='competera'

ENV DJANGO_SETTINGS_MODULE='app.settings'

ENV DB_ENGINE='django.db.backends.postgresql_psycopg2'
ENV DB_PORT='5432'
ENV DB_HOST='db'
ENV DB_NAME=$PROJECT_NAME
ENV DB_USER=$PROJECT_NAME
ENV DB_PASSWORD=$PROJECT_NAME

ENV SECRET_KEY='Q)J\87#\xb7\x95\x14\xa0V\x1e\xb1h\x94\xdc\n\ghnmnrt\asd'

ENV REDIS_BACKEND='django_redis.cache.RedisCache'
ENV REDIS_LOCATION='cache:6379:0'
ENV REDIS_OPTIONS_CLIENT_CLASS='django_redis.client.DefaultClient'
ENV REDIS_OPTIONS_PARSER_CLASS='redis.connection.HiredisParser'

ENV CACHE_PREFIX=$PROJECT_NAME

ENV OPENWEATHER_API_KEY=6dbaa3fb101c5ab6cbab989d467b073d
