# example from https://github.com/yaroslavprogrammer/django-test-rest
#
# import requests
#
# from django.core.exceptions import ValidationError
# from django.utils.translation import ugettext_lazy as _
#
#
# class SiteAvailableValidator(object):
#     """Validate if site have 200 HEAD OK response"""
#
#     def __call__(self, value):
#         try:
#             response = requests.head(value)
#             response.raise_for_status()
#         except:
#             raise ValidationError(_('Site {} is not available').format(value))
