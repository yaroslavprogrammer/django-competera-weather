from django.db import transaction
from django.utils import timezone

from app.celery import app

from .models import WeatherData, WeatherProviderSettings, WeatherProviderReport


@app.task
def fetch_weather_from_openweather(settings_id):
    # TODO: use more general report mechanisms

    settings = WeatherProviderSettings.objects.get(id=settings_id)
    provider = settings.provider()
    date = timezone.now().date()

    report = WeatherProviderReport(settings=settings)
    status = report.SUCCESS

    try:
        with transaction.atomic():
            report.progress = report.RUNNING
            report.save()

            for city in settings.cities.all():
                for data in provider.forecast(city):
                    # TODO: move predicted to save (timezone incapsulated)
                    predicted = date < data['date_for'].date()
                    data, created = WeatherData.objects.get_or_create(
                        date_for=data['date_for'],
                        city_id=city.id,
                        defaults={
                            'temp': data['temp'],
                            'temp_max': data['temp_max'],
                            'temp_min': data['temp_min'],
                            'pressure': data['pressure'],
                        },
                        predicted=predicted
                    )

                    if created and not predicted:
                        WeatherData.find_and_calc_prediction(data)
    except Exception as exc:
        status = report.FAILED
        # TODO: more info (traceback)
        report.error_message = str(exc)

    report.status = status
    report.progress = report.COMPLETED
    report.save()
