from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = i18n_patterns(
    url(r'^admin/', include(admin.site.urls)),

    # url(r'api/', include(
        # 'app.components.sites.api_urls', namespace='sites_api'))
)

# i18n independent patterns

# urlpatterns += (
#     url(),
# )

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
