from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import WeatherData, WeatherProviderSettings, WeatherProviderReport


@admin.register(WeatherData)
class WeatherDataAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = (
        'date_for', 'city', 'temp', 'get_temperature_display',
        'predicted', 'prediction_error')
    search_fields = ('city__name',)
    list_filter = ('predicted',)

    def save_model(self, request, obj, form, change):
        # try to recalc in admin
        super().save_model(request, obj, form, change)
        obj.calculate_prediction()


@admin.register(WeatherProviderSettings)
class WeatherProviderSettingsAdmin(admin.ModelAdmin):
    list_display = ('data_provider', 'cities_count', 'reports_count')
    actions = ('fetch',)

    # TODO: move to aggregate and use 'short_description' with admin sort order
    def cities_count(self, obj):
        return obj.cities.count()

    def reports_count(self, obj):
        return obj.reports.count()

    def fetch(self, request, queryset):
        for obj in queryset:
            obj.fetch()
            self.message_user(request, '{}: {}'.format(
                _('Added fetching task to queue'), obj))
    fetch.short_description = _('Fetch data using provider')


@admin.register(WeatherProviderReport)
class WeatherProviderReportAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'status', 'progress', 'settings')
    list_filter = ('settings', 'status', 'progress')
