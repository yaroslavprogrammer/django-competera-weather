from urllib.parse import urljoin

import requests
from datetime import datetime


class OpenWeatherMap:
    """Provider for fetching info weather info"""

    SETTINGS = {
        'key': 'define',
        'url': 'http://api.openweathermap.org/data/2.5/'
    }
    NAME = 'openweathermap'
    USE_DJANGO_SETTINGS = True

    def __init__(
            self, settings=None):
        if settings is None and self.USE_DJANGO_SETTINGS:
            from django.conf import settings
            self.settings = settings.WEATHER['providers'][self.NAME]
        else:
            raise ValueError(
                "Provide settings like this {}".format(self.SETTINGS))

    def get(self, url, **params):
        params.update(APPID=self.settings['key'])
        url = urljoin(self.settings['url'], url)
        return requests.get(url, params=params).json()

    def weather(self, city=''):
        # TODO: implement adaptation for api
        data = self.get('weather', q=city)
        return data

    def forecast(self, city):
        data = self.get('forecast', q=city)
        for weather in data['list']:
            yield {
                'date_for':  datetime.utcfromtimestamp(weather['dt']),
                'temp': weather['main']['temp'],
                'temp_max': weather['main']['temp_max'],
                'temp_min': weather['main']['temp_min'],
                'pressure': weather['main']['pressure']
            }
