from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class TimeStampMixin(models.Model):
    created_at_in_db = models.DateTimeField(
        _('created in db'), auto_now_add=True, null=True)
    created_at = models.DateTimeField(
        _('created at'), default=timezone.now, editable=False)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        abstract = True
        ordering = ('-created_at',)
