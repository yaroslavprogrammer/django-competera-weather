from django.db import models
from django.utils.translation import ugettext_lazy as _

from app.helpers.mixins import TimeStampMixin


class Country(TimeStampMixin):
    name = models.CharField(_('name'), max_length=255)

    class Meta(TimeStampMixin.Meta):
        verbose_name = _('country')
        verbose_name_plural = _('country')

    def __str__(self):
        return self.name


class City(TimeStampMixin):
    name = models.CharField(_('name'), max_length=255)
    country = models.ForeignKey(Country)

    class Meta(TimeStampMixin.Meta):
        verbose_name = _('city')
        verbose_name_plural = _('city')

    def __str__(self):
        return self.name
