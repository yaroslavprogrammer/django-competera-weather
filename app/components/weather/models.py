from decimal import Decimal

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

from app.helpers.mixins import TimeStampMixin
from app.components.cities.models import City

from .providers import OpenWeatherMap


class WeatherProviderMixin(models.Model):
    OPENWEATHERMAP = 0
    PROVIDER_CHOICES = (
        (OPENWEATHERMAP, _('OpenWeatherMap')),
    )

    data_provider = models.PositiveIntegerField(
        _('provided by'), choices=PROVIDER_CHOICES,
        default=OPENWEATHERMAP
    )

    class Meta:
        abstract = True

    def provider(self):
        if self.data_provider == self.OPENWEATHERMAP:
            return OpenWeatherMap()

class UnitsMixin(models.Model):
    # TODO: implement unit conversion

    METRIC = 0
    UNIT_CHOICES = (
        (METRIC, _('Metric')),
    )

    unit = models.PositiveIntegerField(
        _('units'), choices=UNIT_CHOICES, default=METRIC)

    class Meta:
        abstract = True

    def get_temperature_display(self):
        if self.unit == self.METRIC:
            return _('c°')
    get_temperature_display.short_description = _('Unit')


class WeatherData(
        TimeStampMixin, UnitsMixin):
    date_for = models.DateTimeField(_('date'), null=True)
    temp = models.DecimalField(
        _('temperature'), decimal_places=2, max_digits=5, db_index=True)
    temp_min = models.DecimalField(
        _('minimum temperature'), decimal_places=2, max_digits=5,
        blank=True, null=True)
    temp_max = models.DecimalField(
        _('maximum temperature'), decimal_places=2, max_digits=5,
        blank=True, null=True)
    pressure = models.PositiveIntegerField(
        _('pressure'), blank=True, null=True)
    city = models.ForeignKey(City)

    predicted = models.BooleanField(_('predicted'), default=False)
    prediction_error = models.DecimalField(
        _('prediction error percent'), decimal_places=2, max_digits=4,
        null=True, blank=True
    )
    auto_predict = models.BooleanField(_('auto predict'), default=True)
    predicted_for = models.ManyToManyField('self', blank=True)

    class Meta:
        verbose_name = _('data')
        verbose_name_plural = _('data')

    def __str__(self):
        return '{} - {}: {} {}'.format(
            self.date_for.strftime('%Y-%m-%d'), self.city, self.temp,
            self.get_temperature_display())

    def __repr__(self):
        return '<Weather: {} temp: {}>'.format(self.date_for, self.temp)


    @classmethod
    def find_and_calc_prediction(cls, data):
        if data.predicted:
            return

        objs = cls.objects.filter(
            date_for=data.date_for,
            predicted=True
        )
        for obj in objs:
            obj.predicted_for.add(data)
            obj.calculate_prediction()

    def calculate_prediction(self, recalc=False):
        # get real data if we linked it
        # TODO: optimize save to lists in some steps not of queryset may be slow
        predicted_for = self.predicted_for.all()

        # if we don't have it check if we want to fetch by auto detect option
        if not predicted_for and self.auto_predict:
            predicted_for = self.__class__.objects.filter(
                date_for=self.date_for,
                predicted=False,
            )

            # exclude self if we already saved instance
            if self.id:
                predicted_for = predicted_for.exclude(id=self.id)

            self.predicted_for.add(*predicted_for)

        # we don't have real data and this is not predicted data
        if not predicted_for or not self.predicted:
            return

        # we don't have preidction_error or don't want to recalc it
        if not (self.prediction_error is None or recalc):
            return

        # get avg of real temp
        temp = sum(data.temp for data in predicted_for) / len(predicted_for)
        # get abs percent of diff
        error = abs(self.temp / temp - Decimal('1'))

        self.prediction_error = error.quantize(Decimal('.01'))

        super().save(update_fields=('prediction_error',))


class WeatherProviderSettings(WeatherProviderMixin):
    cities = models.ManyToManyField(City, blank=False)

    class Meta:
        verbose_name = _('settings')
        verbose_name_plural = _('settings')

    def fetch(self):
        from .tasks import fetch_weather_from_openweather

        if self.data_provider == self.OPENWEATHERMAP:
            fetch_weather_from_openweather.apply_async(args=[self.id])

    def __repr__(self):
        return '<WeatherProvider: for cities [{}]>'.format(self.cities.count())

    def __str__(self):
        return '{}: [{}]'.format(
            self.get_data_provider_display(),
            ','.join([city.name for city in self.cities.all()]))


class WeatherProviderReport(TimeStampMixin):
    SUCCESS, FAILED, UNDEFINED = 0, 1, 2
    STATUS_CHOICES = (
        (SUCCESS, _('Success')),
        (FAILED, _('Failed')),
        (UNDEFINED, _('Undefined'))
    )

    COMPLETED, RUNNING, STARTED = 0, 1, 2
    PROGRESS_CHOICES = (
        (COMPLETED, _('Completed')),
        (RUNNING, _('Running')),
        (STARTED, _('Started'))
    )

    status = models.PositiveIntegerField(
        _('Status'), choices=STATUS_CHOICES, default=UNDEFINED)
    progress = models.PositiveIntegerField(
        _('Progress'), choices=PROGRESS_CHOICES, default=STARTED)
    settings = models.ForeignKey(
        WeatherProviderSettings, related_name='reports')
    raw_data = JSONField(_('raw data'), null=True, blank=True)
    error_message = models.TextField(_('exception'), null=True, blank=True)

    class Meta:
        verbose_name = _('report')
        verbose_name_plural = _('reports')

    def __repr__(self):
        return '<Report: {} - {}> {}: {}'.format(
            self.id, self.created_at, self.get_status_display(),
            self.get_progress_display())
